# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test
# and build any application. Docker, when used with GitLab CI, runs each job
# in a separate and isolated container using the predefined image that is set
# up in .gitlab-ci.yml. In this case we use the latest python docker image to
# build and test this project.
image: nexus.engageska-portugal.pt/ska-docker/tango-builder:latest

variables:
  DOCKER_DRIVER: overlay2

services:
  - docker:dind

# cache is used to specify a list of files and directories which should be
# cached between jobs. You can only use paths that are within the project
# workspace. If cache is defined outside the scope of jobs, it means it is set
# globally and all jobs will use that definition
cache:
  paths:

# before_script is used to define the command that should be run before all
# jobs, including deploy jobs, but after the restoration of artifacts. This
# can be an array or a multi-line string.
before_script:
  - docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_HOST

# The YAML file defines a set of jobs with constraints stating when they
# should be run. You can specify an unlimited number of jobs which are defined
# as top-level elements with an arbitrary name and always have to contain at
# least the script clause. In this case we have only the test job which
# produce an artifacts (it must be placed into a directory called "public").
# It is also specified that only the master branch will be subject of this
# job.

stages:
  - .pre
  - build
  - test
  - linting
  - pages
  - increment
  - push_image
  - .post

build:
  tags:
    - docker-executor
  stage: build
  script:
    - sed -i 's/"secret":""/"secret":"'$SECRET'"/g' config.json
    - make -f ska/Makefile -C . build

test:
  tags:
    - docker-executor
  stage: test
  script:
    - make -f ska/Makefile -C . test
  artifacts:
    paths:
      - build
    expire_in: 7 days

linting:
  tags:
    - docker-executor
  stage: linting
  script:
    - make -f ska/Makefile -C . lint
  artifacts:
    paths:
      - build
    expire_in: 7 days

pages:
  when: always
  tags:
    - docker-executor
  stage: pages
  dependencies:
    - test
    - linting
  script:
    - mkdir public
    - cp -R build public
  artifacts:
    paths:
      - public
    expire_in: 7 days

auto_increment_build_number:
  stage: increment
  variables:
    VAR_NAME: BUILD_NUMBER
    TOKEN: ${CI_PIPELINE_IID_TOKEN}
    GITLAB_URL: "https://gitlab.com"
  before_script:
  - apt install -y curl jq
  script:
  - "VAR=$(curl -s -f  --header \"PRIVATE-TOKEN: ${TOKEN}\" \"${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/variables/${VAR_NAME}\" | jq  -r '.value' ) "
  - let VAR=VAR+1
  - "curl -s -f --request PUT --header \"PRIVATE-TOKEN: ${TOKEN}\" \"${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/variables/${VAR_NAME}\" --form \"value=${VAR}\" "

push_image:
  tags:
    - docker-executor
  stage: push_image
  script:
    - sed -i 's/"secret":""/"secret":"'$SECRET'"/g' config.json
    - make -f ska/Makefile -C . build
    - make -f ska/Makefile -C . push

create ci metrics:
  stage: .post
  when: always
  image: nexus.engageska-portugal.pt/ska-docker/tango-builder:latest
  tags:
    - docker-executor
  script:
    # Gitlab CI badges creation
    - curl -s https://gitlab.com/ska-telescope/ci-metrics-utilities/raw/master/scripts/ci-badges-func.sh | sh
  artifacts:
    paths:
      - public
