# TangoGQL: SKA CI/CD

## Description

This directory contains files used to integrate the TangoGQL project with the
SKA build environment and continuous integration server. These files build
TangoGQL using the SKA-standard Pipenv procedure, creating a TangoGQL Docker
image that packages TangoGQL inside the standard SKA Python container
environment.

## Usage

### Prerequisites

The SKA build procedure requires the following dependencies. Ensure they are
installed on your system before trying to build images.

* GNU Make (v4.1+)
* Docker (v18+)

### Building a new TangoGQL Docker image

From the root directory of this project, execute

```shell script
$ make -f ska/Makefile -C . build
```
